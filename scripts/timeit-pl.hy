;
; n23 - data acquisition and processing framework
;
; Copyright (C) 2013-2024 by Artur Wroblewski <wrobell@riseup.net>
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;

"""
Microbenchmark to measure speed of execution of pipeline functions.

Pipeline functions are executed by N23 scheduler to transform and filter
source data. They can be composed by different means. This script measures
execution of functions composed with 

- N23 `n23->` macro
- toolz `thread_first` function
- cytoolz `thread_first` function

For example::

  $ PYTHONPATH=. hy ./scripts/timeit-pl.hy
  defn/p 0.3382 us
  toolz 1.1287 us
  cytoolz 0.6423 us
"""

(import cytoolz [functoolz :as cfz]
        operator :as op
        toolz [functoolz :as tfz]
        time [monotonic])

(require n23.pipeline [n23-> ?->])

(setv N 10000)

(setv pl-hy (n23-> str.split
                   (get 0)
                   float
                   (?-> > 0)))

(setv pl-tpy (fn [v]
              (tfz.thread_first
                v
                str.split
                (op.itemgetter 0)
                float
                (fn [v] (if (> v 0) v None)))))

(setv pl-cpy (fn [v]
              (cfz.thread_first
                v
                str.split
                (op.itemgetter 0)
                float
                (fn [v] (if (> v 0) v None)))))

(assert (= (pl-hy "1 2 3") 1.0))
(assert (= (pl-cpy "1 2 3") 1.0))
(assert (= (pl-tpy "1 2 3") 1.0))
(assert (is (pl-hy "0 2 3") None))
(assert (is (pl-cpy "0 2 3") None))
(assert (is (pl-tpy "0 2 3") None))

(setv t1 (monotonic))
(for [i (range N)] (pl-hy "1 2 3"))
(setv t2 (monotonic))
(print (str.format "n23-> {:.4f} us" (/ (* (- t2 t1) 1e6) N)))

(setv t1 (monotonic))
(for [i (range N)] (pl-tpy "1 2 3"))
(setv t2 (monotonic))
(print (str.format "toolz {:.4f} us" (/ (* (- t2 t1) 1e6) N)))

(setv t1 (monotonic))
(for [i (range N)] (pl-cpy "1 2 3"))
(setv t2 (monotonic))
(print (str.format "cytoolz {:.4f} us" (/ (* (- t2 t1) 1e6) N)))

; vim: sw=2:et:ai
