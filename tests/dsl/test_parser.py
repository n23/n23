#
# n23 - data acquisition and processing framework
#
# Copyright (C) 2013-2024 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import typing as tp

from n23.dsl import parse_dsl

from n23.app import app_config_value
from n23.app.types import ApplicationConfig

from n23.storage.config.schema import PARSER as PARSER_DB_CONFIG
from n23.storage.config.types import Config as DatabaseConfig

import pytest

@pytest.mark.parametrize(
    'name, expected',
    [['device', '/dev/random'],
     ['db-uri', 'postgresql://localhost/some-db'],
     ['weights', [300, 50]],
     ['other-weights', (301.5, 51.2)],
     ['tags', {'tag1': 'val1', 'tag2': 'val2'}]])
def test_app_config_parser(
        app_config: ApplicationConfig, name: str, expected: tp.Any
) -> None:
    """
    Test getting value from N23 application configuration.
    """
    assert app_config_value(app_config, 'sect1', name) == expected

def test_db_config_parser() -> None:
    """
    Test N23 database config parser.
    """
    data = """
(storage
  (version "1.0.0")
  (role some-role)
  (extensions (timescaledb postgis)))

(entities
  (entity table_1
    (partition-by name)
    (columns
      (name "varchar(20)")
      (col1 float4)
      (col2 integer8))))

(sql #[[
some-sql to extend the schema
]])
    """
    result = parse_dsl(data, PARSER_DB_CONFIG, DatabaseConfig)
    assert result.storage.version == '1.0.0'
    assert result.storage.role == 'some-role'
    assert result.storage.extensions == ('timescaledb', 'postgis')
    assert result.entities[0].name == 'table_1'
    assert result.entities[0].partition_by == 'name'
    assert result.entities[0].chunk == '1day'

    assert result.entities[0].columns[0].name == 'name'
    assert result.entities[0].columns[0].type == 'varchar(20)'
    assert not result.entities[0].columns[0].nullable

    assert result.entities[0].columns[1].name == 'col1'
    assert result.entities[0].columns[1].type == 'float4'
    assert not result.entities[0].columns[1].nullable

    assert result.entities[0].columns[2].name == 'col2'
    assert result.entities[0].columns[2].type == 'integer8'
    assert not result.entities[0].columns[2].nullable

# vim:sw=4:et:ai
