#
# n23 - data acquisition and processing framework
#
# Copyright (C) 2013-2023 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import hy  # noqa: F401
from pathlib import Path

from n23.app import ApplicationConfig
from n23.dsl import parse_dsl
from n23.app.config_schema import PARSER as PARSER_APP_CONFIG

import pytest

def pytest_collect_file(file_path: Path, parent: Path) -> pytest.Module | None:
    if file_path.name.startswith('test_') and file_path.suffix == '.hy':
        return pytest.Module.from_parent(parent, path=file_path)
    else:
        return None

@pytest.fixture
def app_config() -> ApplicationConfig:
    data = """
(section sect1
  (device "/dev/random")
  (db-uri "postgresql://localhost/some-db")
  (weights [300 50])
  (other-weights #(301.5 51.2))
  (tags {"tag1" "val1" "tag2" "val2"}))
    """
    return parse_dsl(
        data, PARSER_APP_CONFIG, ApplicationConfig, positional=True
    )

# vim: sw=4:et:ai
