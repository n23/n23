#
# n23 - data acquisition and processing framework
#
# Copyright (C) 2013-2023 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import typing as tp

from n23.error import ApplicationError
from n23.storage.api import storage_from_uri, _pg_from_uri
from n23.storage.pg import Database

import pytest

def test_from_uri_error() -> None:
    """
    Test if application error is raised when storage is not recognized.
    """
    with pytest.raises(ApplicationError) as ex_ctx:
        storage_from_uri('http://invalid')
    assert 'Unknown storage: http' == str(ex_ctx.value)

def test_pg_from_uri() -> None:
    """
    Test creating PostgreSQL storage instance from URI.
    """
    uri = 'postgresql://localhost/dbname'
    db = tp.cast(Database, _pg_from_uri(uri))

    assert db.db_uri == uri

# vim: sw=4:et:ai
