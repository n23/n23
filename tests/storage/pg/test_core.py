#
# n23 - data acquisition and processing framework
#
# Copyright (C) 2013-2025 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ruff: noqa: SLF001

"""
Unit tests for basic database functions.
"""

import asyncio
import asyncpg  # type: ignore
import typing as tp
from contextlib import contextmanager
from datetime import datetime, timezone
from functools import partial

from n23.storage.pg import core as pg_core
from n23 import TaskResult

import pytest
from unittest import mock

@contextmanager
def patch_connect(
        side_effect: tp.Any | None=None
    ) -> tp.Iterator[mock.AsyncMock]:
    """
    Context manager to patch `connect` coroutine.
    """
    with mock.patch.object(pg_core, 'connect', mock.MagicMock()) as mock_connect:
        conn = mock.AsyncMock(spec=asyncpg.Connection)
        task = mock_connect()
        task.__aenter__.return_value = conn
        task.__aenter__.side_effect = side_effect

        yield conn

@pytest.mark.asyncio
async def test_db_add_entity() -> None:
    """
    Test adding entity to the data storage.
    """
    db = pg_core.Database('test')
    db.add_entity('t1', ('value',))

    assert 't1' in db._writers

    writer = db._writers['t1']
    assert writer._table == 't1'
    assert writer._columns == ('time', 'value')
    assert isinstance(writer, pg_core.TableWriter)

def test_db_add_entity_batch() -> None:
    """
    Test adding entity to the data storage with batch semantics.
    """
    db = pg_core.Database('test')
    db.add_entity('t1', ('value',), batch=True)

    assert 't1' in db._writers

    writer = db._writers['t1']
    assert writer._table == 't1'
    assert writer._columns == ('time', 'value')
    assert isinstance(writer, pg_core.TableBatchWriter)

@pytest.mark.asyncio
async def test_db_write_name_single() -> None:
    """
    Test writing data with single value in task result name (room1)
    """
    with patch_connect() as conn:
        db = pg_core.Database('test')
        db.add_entity('t1', ('name', 'value'))

        v1 = TaskResult(1, 't1/room1', 'v1')
        v2 = TaskResult(2, 't1/room1', 'v2')

        await db.write(v1)
        await db.write(v2)

        dt = partial(datetime, 1970, 1, 1, 0, 0 , tzinfo=timezone.utc)
        expected = [
            mock.call(
                't1',
                records=[(dt(1), 'room1', 'v1')],
                columns=('time', 'name', 'value'),
            ),
            mock.call(
                't1',
                records=[(dt(2), 'room1', 'v2')],
                columns=('time', 'name', 'value'),
            ),
        ]
        assert conn.copy_records_to_table.call_args_list == expected

@pytest.mark.timeout(7)  # default sleep on connection error is 5s
@pytest.mark.asyncio
async def test_db_write_error() -> None:
    """
    Test writing data with error
    """
    # simulate connection error with OSError exception
    side_effect = [OSError, None]
    with patch_connect(side_effect) as conn:
        # replace null with connection, so write can happen
        side_effect[-1] = conn

        db = pg_core.Database('test')
        db.add_entity('t1', ('name', 'value'))
        await db.write(TaskResult(1, 't1/room1', 'v1'))

        dt = partial(datetime, 1970, 1, 1, 0, 0 , tzinfo=timezone.utc)
        expected = [
            mock.call(
                't1',
                records=[(dt(1), 'room1', 'v1')],
                columns=('time', 'name', 'value'),
            ),
        ]
        assert conn.copy_records_to_table.call_args_list == expected

@pytest.mark.asyncio
async def test_db_write_name_multiple() -> None:
    """
    Test writing data with multiple values (x1, x2) in task result name.
    """
    with patch_connect() as conn:
        db = pg_core.Database('test')
        cols = ('name', 'type', 'value')
        db.add_entity('t1', cols)

        v1 = TaskResult(1, 't1/room1/x1', 'v1')
        v2 = TaskResult(2, 't1/room1/x2', 'v2')

        await db.write(v1)
        await db.write(v2)

        dt = partial(datetime, 1970, 1, 1, 0, 0 , tzinfo=timezone.utc)
        expected = [
            mock.call(
                't1',
                records=[(dt(1), 'room1', 'x1', 'v1')],
                columns=('time', *cols)
            ),
            mock.call(
                't1',
                records=[(dt(2), 'room1', 'x2', 'v2')],
                columns=('time', *cols)
            ),
        ]
        assert conn.copy_records_to_table.call_args_list == expected

@pytest.mark.asyncio
async def test_db_write_multiple() -> None:
    """
    Test writing data with multiple values in taks result name (x1, x2) and
    in task result value (v[ab][12]).
    """
    with patch_connect() as conn:
        db = pg_core.Database('test')
        cols = ('name', 'type', 'value_1', 'value_2')
        db.add_entity('t1', cols)

        v1 = TaskResult(1, 't1/room1/x1', ('va1', 'va2'))
        v2 = TaskResult(2, 't1/room1/x2', ('vb1', 'vb2'))

        await db.write(v1)
        await db.write(v2)

        dt = partial(datetime, 1970, 1, 1, 0, 0 , tzinfo=timezone.utc)
        expected = [
            mock.call(
                't1',
                records=[(dt(1), 'room1', 'x1', 'va1', 'va2')],
                columns=('time', *cols)
            ),
            mock.call(
                't1',
                records=[(dt(2), 'room1', 'x2', 'vb1', 'vb2')],
                columns=('time', *cols)
            ),
        ]
        assert conn.copy_records_to_table.call_args_list == expected

@pytest.mark.timeout(3)
@pytest.mark.asyncio
async def test_db_write_batch() -> None:
    """
    Test saving data into a table in batch mode.
    """
    with patch_connect() as conn:
        db = pg_core.Database('test')
        db._pool = asyncio.sleep(0)

        cols = ('name', 'type', 'value_1', 'value_2')
        db.add_entity('t1', cols, batch=True)

        v1 = TaskResult(1, 't1/room1/x1', ('va1', 'va2'))
        v2 = TaskResult(2, 't1/room1/x2', ('vb1', 'vb2'))

        await db.write(v1)
        await db.write(v2)

        writer = tp.cast(pg_core.TableBatchWriter, db._writers['t1'])
        dt = partial(datetime, 1970, 1, 1, 0, 0 , tzinfo=timezone.utc)

        expected_queue = [
            (dt(1), 'room1', 'x1', 'va1', 'va2'),
            (dt(2), 'room1', 'x2', 'vb1', 'vb2'),
        ]
        assert list(writer._queue) == expected_queue

        with mock.patch.object(writer, '_queue') as mock_queue:
            await db.flush()

            expected = [
                mock.call(
                    't1',
                    records=mock_queue,
                    columns=('time', *cols)
                ),
            ]
            assert conn.copy_records_to_table.call_args_list == expected

            # queue is cleared after successfull data save into a table
            assert mock_queue.clear.call_count == 1

@pytest.mark.timeout(3)
@pytest.mark.asyncio
async def test_db_write_batch_on_full() -> None:
    """
    Test saving data into a table in batch mode when buffer is full.
    """
    with patch_connect() as conn:
        pool = mock.AsyncMock()
        writer = pg_core.TableBatchWriter(
            pool,
            't1',
            ('time', 'name', 'value'),
            max_len=2,
        )

        await writer.write((1, 'abc', 10))
        await writer.write((2, 'abc', 20))
        await writer.write((3, 'abc', 30))

        expected_queue = [
            (3, 'abc', 30),
        ]
        assert list(writer._queue) == expected_queue
        assert conn.copy_records_to_table.call_count == 1

@pytest.mark.asyncio
async def test_db_write_batch_error() -> None:
    """
    Test saving data into a table in batch mode with error
    """
    # simulate connection error with OSError exception
    side_effect = [OSError('simulated failure'), None]
    with patch_connect(side_effect) as conn:
        # replace null with connection, so write can happen
        side_effect[-1] = conn

        db = pg_core.Database('test')
        db._pool = mock.AsyncMock()

        cols = ('name', 'type', 'value_1', 'value_2')
        db.add_entity('t1', cols, batch=True)

        v1 = TaskResult(1, 't1/room1/x1', ('va1', 'va2'))
        v2 = TaskResult(2, 't1/room1/x2', ('vb1', 'vb2'))

        await db.write(v1)
        await db.write(v2)

        writer = tp.cast(pg_core.TableBatchWriter, db._writers['t1'])
        dt = partial(datetime, 1970, 1, 1, 0, 0 , tzinfo=timezone.utc)

        expected_queue = [
            (dt(1), 'room1', 'x1', 'va1', 'va2'),
            (dt(2), 'room1', 'x2', 'vb1', 'vb2'),
        ]
        assert list(writer._queue) == expected_queue

        # error on first attempt, success on 2nd attempt, then stop
        with mock.patch.object(writer, '_queue') as mock_queue:
            # do not execute on finnally statement in `flush_on_event`
            mock_queue.__bool__.side_effect = [True, True, False]

            # flush, there is error OSError
            await db.flush()
            # flush again
            await db.flush()

            expected = [
                mock.call(
                    't1',
                    records=mock_queue,
                    columns=('time', *cols)
                ),
            ]
            assert conn.copy_records_to_table.call_args_list == expected

            # queue is cleared after successfull data save into a table
            assert mock_queue.clear.call_count == 1

def test_is_disconnected_closed() -> None:
    """
    Test if PostgreSQL interface error for closed connection can be
    detected.
    """
    ex = asyncpg.InterfaceError('...: the underlying connection is closed')
    assert pg_core.is_disconnected(ex)

def test_is_disconnected_back_to_pool() -> None:
    """
    Test if PostgreSQL interface error for connection released to
    connection pool is detected.
    """
    ex = asyncpg.InterfaceError(
        '...: connection has been released back to the pool'
    )
    assert pg_core.is_disconnected(ex)

def test_db_uri_scheme_host() -> None:
    """
    Test adding db URI scheme and host.
    """
    result = pg_core.to_db_uri('some-db')
    assert 'postgresql://localhost/some-db' == result

def test_db_uri_scheme_host_with_user() -> None:
    """
    Test adding db URI scheme and host for db with user.
    """
    result = pg_core.to_db_uri('user:pass@localhost/some-db')
    assert 'postgresql://user:pass@localhost/some-db' == result

def test_db_uri_scheme() -> None:
    """
    Test adding db URI scheme.
    """
    result = pg_core.to_db_uri('10.0.0.1/some-db')
    assert 'postgresql://10.0.0.1/some-db' == result

def test_db_uri_dbname() -> None:
    """
    Test replacing db URI database name.
    """
    result = pg_core.to_db_uri('postgresql://localhost/some-db', alt_db='other-name')
    assert 'postgresql://localhost/other-name' == result

@pytest.mark.asyncio
async def test_connect_db_pool() -> None:
    """
    Test connecting to database with existing connection pool.
    """
    conn = mock.MagicMock()
    pool = mock.MagicMock()
    pool.acquire.return_value = mock.MagicMock()
    pool.acquire.return_value.__aenter__.return_value = conn
    async with pg_core._connect_db_pool(pool) as conn:
        pass

    # check that connection is acquired using the connection pool
    pool.acquire.assert_called_once_with()

    # check that transaction is created
    conn.transaction.assert_called_once_with()

@pytest.mark.asyncio
async def test_catch_conn_error_from_pool() -> None:
    """
    Test catching connection error raised by connection pool.
    """
    task = mock.AsyncMock(side_effect=[OSError])
    result = await pg_core.catch_conn_error(task())
    # no exception as raised error is connection error, but false is
    # returned
    assert not result

@pytest.mark.asyncio
async def test_catch_conn_error_on_exec() -> None:
    """
    Test catching connection error raised during command execution.
    """
    ex = asyncpg.InterfaceError('...: the underlying connection is closed')
    task = mock.AsyncMock(side_effect=[ex])
    result = await pg_core.catch_conn_error(task())
    # no exception as raised error is connection error, but false is
    # returned
    assert not result

@pytest.mark.asyncio
async def test_catch_conn_error_fail() -> None:
    """
    Test exceptions other than connection error are not swallowed when
    protecting from connection errors.
    """
    task = mock.AsyncMock(side_effect=[ValueError()])
    with pytest.raises(ValueError):
        await pg_core.catch_conn_error(task())

@pytest.mark.asyncio
async def test_create_pool(mocker) -> None:  # type: ignore
    """
    Test creating connection pool.
    """
    mock_create_pool = mocker.patch('asyncpg.create_pool')
    mock_create_pool().__aenter__ = mock.AsyncMock()
    mock_create_pool().__aexit__ = mock.AsyncMock()

    async with pg_core.create_pool('test'):
        mock_create_pool.assert_called_once_with(
            'test', min_size=1, max_size=1
        )

# vim: sw=4:et:ai
