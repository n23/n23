;
; n23 - data acquisition and processing framework
;
; Copyright (C) 2013-2024 by Artur Wroblewski <wrobell@riseup.net>
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;

(import hy.pyops [< > * +]
        functools [partial]
        logging

        n23
        n23.pipeline [CTX-STREAM-MERGE-DEBUG
                      fmap
                      identity
                      ctor-streams-merge]
        pytest)

(require n23.pipeline [n23-> fmap-> ?->]
         hyrule [->>])

(setv logger (logging.getLogger))

(defn [(pytest.mark.parametrize "func, args, value, expected"
        [[> [5] 6 6]
         [> [5] 4 None]])]
  test-filter [func args value expected]
  #[[
  Test pipeline filter.
  ]]
  (setv result (hy.eval `(?-> value func ~@args)))
  (assert (= result expected)))

(defn test-pipeline-filter-end []
  #[[
  Test pipeline with filter at the end of the pipeline.
  ]]
  (setv pl (n23-> str.split
                  (get 0)
                  float
                  (?-> > 0)))

  (assert (= (pl "2 3") 2.0))
  (assert (is (pl "0 3") None)))

(defn test-pipeline-filter-middle []
  #[[
  Test pipeline with filter in the middle of the pipeline.
  #]]
  (setv pl (n23-> str.split
                  (get 0)
                  int
                  (?-> > 0)
                  (+ 1)))

  (assert (= (pl "3 5") 4.0))
  (assert (is (pl "0 3") None)))

(defn test-fmap []
  #[[
  Test functor execution over task result with via `fmap` function.
  #]]
  (setv v (n23.TaskResult 1.0 "name" 3))
  (setv r ((fmap *) v 4))
  (assert (= r.time 1.0))
  (assert (= r.name "name"))
  (assert (= r.value 12)))

(defn test-fmap-> []
  #[[
  Test functor execution over task result via `fmap->` macro.
  #]]
  (setv v (n23.TaskResult 1.0 "name" 3))
  (setv r (fmap-> v * 4))
  (assert (= r.time 1.0))
  (assert (= r.name "name"))
  (assert (= r.value 12)))

(defn [(pytest.mark.parametrize "fp, vp, ep, fh, vh, eh"
        [['((fmap-> * 2)) 10 20 '((fmap-> * 3)) 15 45]
         ['((fmap-> ?-> > 0)) 10 10 '((fmap-> ?-> > 0)) 15 15]
         ['((fmap-> ?-> > 0)) 0 None '((fmap-> ?-> > 0)) 0 None]
         ['((fmap-> ?-> > 0) (fmap-> * 2)) 10 20
          '((fmap-> ?-> > 0) (fmap-> * 3)) 14 42]
         ['((fmap-> ?-> > 0) (fmap-> * 2)) 0 None
          '((fmap-> ?-> > 0) (fmap-> * 2)) 0 None]])]
  test-n23-split [fp vp ep fh vh eh]
  #[[
  Test splitting N23 task result into multiple task results.
  #]]
  (setv env (locals))
  (setv f (hy.eval `(n23-> [("pressure" ~@fp)
                            ("temperature" identity)
                            ("humidity" ~@fh)])
                   :locals env))
  (setv [r1 r2 r3] (f (n23.TaskResult 11 "handle" [vp 12 vh])))
  (if (is ep None)
    (assert (is r1 None))
    (do
      (assert (= r1.time 11))
      (assert (= r1.name "pressure"))
      (assert (= r1.value ep))))

  (assert (= r2.time 11))
  (assert (= r2.name "temperature"))
  (assert (= r2.value 12))

  (if (is eh None)
    (assert (is r3 None))
    (do
      (assert (= r3.time 11))
      (assert (= r3.name "humidity"))
      (assert (= r3.value eh)))))

(defn test-n23-argmove []
  """
  Test integrating N23 pipeline definition with thread last macro.
  """
  (setv f (n23-> [("pressure" (fmap-> * 2))
                  ("temperature" identity)
                  ("humidity" (fmap-> * 3))]
                 (->> (map (fmap (partial + 5))))))

  (setv [r1 r2 r3] (f (n23.TaskResult 11 "handle" [10 12 14])))

  (assert (= r1.time 11))
  (assert (= r1.name "pressure"))
  (assert (= r1.value 25))

  (assert (= r2.time 11))
  (assert (= r2.name "temperature"))
  (assert (= r2.value 17))

  (assert (= r3.time 11))
  (assert (= r3.name "humidity"))
  (assert (= r3.value 47)))

(defn [(pytest.mark.parametrize
         "stream, expected, left_count, right_count"
         [[[[[1 5.0] None]] [None None] 1 0]
          [[[None [1 5.0]]] [None None] 0 1]

          ; merge left first, then right
          [[[[1 3.0] [3 5.0]]] [None [2 #(3.0 5.0)]] 0 0]
          ; merge right first, then left  
          [[[None [1 6.0]] [[5 4.0] None]] [[3 #(4.0 6.0)] None] 0 0]

          ; when merge left happens, then first two messages are dropped;
          ; first, let's check that 3 messages inserted into stream are
          ; enqueued; then check that first two are gone after last one is
          ; consumed
          [[[None [0 11]] [None [6 12]] [None [12 13]]] [None None] 0 3]
          [[[None [0 11]] [None [6 12]] [None [12 13]] [[14 26] None]] [[13 #(26 13)] None] 0 0]

          ; send 3 messages to a right stream and then a message to left
          ; stream, message in left stream is in the past, and shall not
          ; affect the messages enqueued for the right stream
          [[[None [6 11]] [None [12 12]] [None [18 13]] [[0 26] None]] [None None] 1 3]])]
      test-stream-merge [stream expected left-count right-count]
      #[[
      Test merging two streams of data.

      Stream parameter is a list of left and right stream messages to merge.

      Expected value is a pair of expected result from last call of left
      and right merge respectively.
      ]]

  (setv [merge-left merge-right] (ctor-streams-merge "merged"
                                                     :delta 5
                                                     :debug True))
  (setv [left-stream right-stream] (CTX-STREAM-MERGE-DEBUG.get))

  (setv r1 None)
  (setv r2 None)
  (for [[m1 m2] stream]
    (when (is-not m1 None)
      (logger.info f"merge-left {m1}")
      (setv [t1 v1] m1) 
      (setv r1 (merge-left (n23.TaskResult t1 "aux1" v1)))
      (logger.info f"merge-left result {r1}"))
    (when (is-not m2 None)
      (logger.info f"merge-right {m2}")
      (setv [t2 v2] m2) 
      (setv r2 (merge-right (n23.TaskResult t2 "aux2" v2)))
      (logger.info f"merge-right result {r2}")))

  (setv [exp1 exp2] expected)
  (if (is exp1 None)
    (assert (is r1 None))
    (do
      (setv [t1 v1] exp1)
      (assert (= r1 (n23.TaskResult t1 "merged" v1)))))

  (if (is exp2 None)
    (assert (is r2 None))
    (do
      (setv [t2 v2] exp2)
      (assert (= r2 (n23.TaskResult t2 "merged" v2)))))

  (assert (= (len left-stream) left-count))
  (assert (= (len right-stream) right-count)))

; vim: sw=2:et:ai
