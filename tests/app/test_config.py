#
# n23 - data acquisition and processing framework
#
# Copyright (C) 2013-2024 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from n23.app import app_config_value, ApplicationConfig, ApplicationConfigError

import pytest

@pytest.mark.parametrize(
    'section, name',
    [('sect1', 'unknown'), ('unknown', 'device')]
)
def test_app_config_value_error(
        app_config: ApplicationConfig, section: str, name: str
) -> None:
    """
    Test if configuration error is raised for uknown configuration item.
    """
    with pytest.raises(ApplicationConfigError):
        app_config_value(app_config, section, name)

@pytest.mark.parametrize(
    'section, name',
    [('sect1', 'unknown'), ('unknown', 'device')]
)
def test_app_config_value_default(
        app_config: ApplicationConfig, section: str, name: str
) -> None:
    """
    Test getting default value from N23 application configuration.
    """
    result = app_config_value(app_config, section, name, default='some-default')
    assert result == 'some-default'

# vim:sw=4:et:ai
