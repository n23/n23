;
; n23 - data acquisition and processing framework
;
; Copyright (C) 2013-2024 by Artur Wroblewski <wrobell@riseup.net>
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;

(import contextlib [AbstractAsyncContextManager]
        n23.app.runner [run-app parse-app run-app-ctx]
        n23.app.types [ApplicationConfig ApplicationConfigSection ApplicationConfigItem]
        pytest)

(defn :async [pytest.mark.asyncio] test-run-app-unknown []
  #[[
  Test if type error is raised on unknown type of application data.
  #]]
  (assert [(pytest.raises TypeError)]
    (run-app 1)))

(defn :async [pytest.mark.asyncio (pytest.mark.timeout 1)]
      test-n23-process-cm []
  #[[
  Test adding asynchronous context manager of a process.
  ]]
  (setv app
        '(do
           (import asyncio
                   contextlib [asynccontextmanager]
                   n23 [TaskResult]
                   n23.fn [identity]
                   unittest [mock])

           ; use sink in the runner and as scheduler sink
           (setv sink (mock.AsyncMock))

           (defn :async runner []
             (for [i (range 10)]
               (do (sink (TaskResult 1.0 "runner" (str.format "run {}" i)))
                   (await (asyncio.sleep 0.06)))))
           (defn :async [asynccontextmanager] process-cm []
             (yield (runner)))

           (defn :async read-data []
             (yield "1 2 3")
             (yield "0 2 3"))
           (n23-add 0.1 "read" read-data identity sink)
           (n23-process (process-cm))))

  (setv config (ApplicationConfig []))
  (setv ctx (parse-app app config))
  (await (ctx.setup))

  (assert (= (len ctx.processes) 0))
  (assert (= (len ctx.process_managers) 1))
  (setv cm (get ctx.process_managers 0))
  (assert (isinstance cm AbstractAsyncContextManager))

  (try
    (await (run-app-ctx ctx config))
    (except* [StopAsyncIteration]
      (setv [names values] (meta-task-result ctx))

      ; add read-data values, and add `runner` process values 
      (setv expected-values [
        "0 2 3" "1 2 3"
        #* (lfor i (range (- (len values) 2)) (str.format "run {}" i))])

      (assert (= names #{"runner" "read"}))
      (assert (= (sorted values) expected-values)))))

(defn :async [pytest.mark.asyncio] test-app-load []
  #[[
  Test example of application reading load data.
  #]]
  (setv app '(do
               (import hy.pyops [<]
                       unittest [mock])

               (defn :async read-data []
                 (yield "1 2 3")
                 (yield "0 2 3")
                 (yield "4 2 3")
                 (yield "5 2 3")
                 (yield "0 2 3"))

               (setv sink (mock.AsyncMock))
               (n23-add
                 0.1
                 "load"
                 read-data
                 (fmap (n23-> str.split
                              (get 0)
                              (float)
                              (?-> > 0)))
                 sink)))

  (setv config (ApplicationConfig []))
  (setv ctx (parse-app app config))
  (try
    (await (run-app-ctx ctx config))
    (except* [StopAsyncIteration]
      (setv [names values] (meta-task-result ctx))
      (assert (= names #{"load"}))
      (assert (= values [1.0 4.0 5.0])))
    (else (assert False "stop iteration not raised"))))

(defn :async [pytest.mark.asyncio (pytest.mark.timeout 1)] test-n23-app-config []
  #[[
  Test configuring N23 application.
  ]]
  (setv app '(do
    (import n23.fn [identity]
            unittest [mock])

    (setv v1 (n23-config-get "sect1" "item1"))
    (setv v2 (n23-config-get "sect2" "item2"))
    (defn :async read-data []
      (yield v1)
      (yield v2))
    (setv sink (mock.AsyncMock))

    (n23-add 0.1 "read" read-data identity sink)))

  (setv config (ApplicationConfig
                 [(ApplicationConfigSection
                    "sect1"
                    [(ApplicationConfigItem "item1" "0 2 3")])
                  (ApplicationConfigSection
                    "sect2"
                    [(ApplicationConfigItem "item2" "1 5 3")])]))
  (setv ctx (parse-app app config))

  (try
    (await (run-app-ctx ctx config))
    (except* [StopAsyncIteration]
      (setv [names values] (meta-task-result ctx))

      (assert (= names #{"read"}))
      (assert (= values ["0 2 3" "1 5 3"])))))

(defn meta-task-result [ctx]
  (setv sink (. (get ctx.scheduler._r_tasks 0) sink))

  (setv args (lfor arg sink.call_args_list (get arg 0 0)))
  (setv names (sfor v args v.name))
  (setv values (lfor v args v.value))
  #(names values))

; vim:sw=2:et:ai
