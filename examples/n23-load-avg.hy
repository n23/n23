;
; n23 - data acquisition and processing framework
;
; Copyright (C) 2013-2025 by Artur Wroblewski <wrobell@riseup.net>
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;

"""
N23 framework example to demonstrate processing of system load.

The example executes asynchronous tasks to

- read system load information from /proc/load
- save data into `load` table of `n23-load` database on localhost

Run with command::

   n23-run examples/n23-load-avg.hy -c examples/n23-load-avg-config.cfg
"""

(import hy.pyops [<])

(defn :async read-load []
  (with [f (open "/proc/loadavg")]
    (while [True] (yield (f.read))
                  (f.seek 0))))

(setv db (n23-sink-from-uri (n23-config-get "database" "uri")))
(db.add_entity "load" ["value"] :batch True)

(n23-add 1 "db-flush" db.flush)
(n23-add 1
         "load"
         read-load
         (fmap (n23-> str.split
                      (get 0)
                      float
                      (?-> > 0)))
         db.write)

(n23-process db)

; vim: sw=2:et:ai
