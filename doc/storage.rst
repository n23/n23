Storage
=======
Configuration
-------------
Storage configuration is defined using Lisp (Hylang) syntax
used.

Configuration items are

storage
    General storage configuration, i.e. roles, extensions.
storage/role
    Role, group or user name allowed to insert and read data from
    a storage like database.
storage/extensions
    Storage specific extensions. For example PostGIS or TimescaleDB for
    PostgreSQL.
entities
    List of entities where data is stored. For example list of tables for
    PostgreSQL.
entity
    Entity information. For example a table for PostgreSQL.
entity/partition-by
    Name of column by which data is partitioned (or chunked or segmented).
entity/columns
    List of fields (or table columns) definitions.

Requirements
------------

General requirements

1. Single writer, multiple readers use case needs to be supported without
   enforcing order of starting of writer and readers. For example, as of today,
   HDF5 requires writer to start before readers, which makes it unsuitable
   storage solution.
2. Autorecovery after improper shutdown should happen without manual
   intervention. Certain data loss is acceptable, i.e. last chunk.

Requirements for large databases

1. Chunking is required to minimize amount of data read for a specific period of
   time. When using proper storage system, a chunk of data can be lost, but
   total data loss should be minimized.
2. Compression minimizes storage requirements. Most of the data is rarely read
   and there is no need to keep it uncompressed. Compression improves
   performance of I/O and might increase overall system performance if CPU can
   compress/decompress faster than system can write/read uncompressed data.

PostgreSQL
----------
Having a stream of data, which partially can be lost, then delay data
write. This will accumulate data being written

- performance wise better as more data is saved in one go
- minimizes storage wear (i.e. SD cards)

PostgreSQL options::

    synchronous_commit = off
    wal_writer_delay = 10s  # maximum value

Bluetooth, CPU and I/O

- bluez can disconnect devices if system performs heavy cpu or I/O
  task - it seems, when response from a Bluetooth device is not received in
  time?
- above applies when using SD card for storage and large PostgreSQL query
  is executed
- set PostgreSQL max workers to lower value to leave a CPU for Bluetooth
- might need other options to help with I/O
- note: how to add higher priority to bluez process?

.. vim: sw=4:et:ai
