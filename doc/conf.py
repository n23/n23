import sys
import os.path
import sphinx_rtd_theme

sys.path.append(os.path.abspath('.'))
sys.path.append(os.path.abspath('doc'))

extensions = [
    'sphinx.ext.autodoc', 'sphinx.ext.doctest', 'sphinx.ext.todo',
    'sphinx.ext.viewcode'
]
project = 'dcmod'
source_suffix = '.rst'
master_doc = 'index'

version = release = '0.1.0' # dcmod.__version__
copyright = 'dcmod Team'

epub_basename = 'dcmod - {}'.format(version)
epub_author = 'dcmod Team'

todo_include_todos = True

html_theme = 'sphinx_rtd_theme'
html_static_path = ['static']
html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]

# vim: sw=4:et:ai
