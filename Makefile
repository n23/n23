include .pymake.mk

SCRIPTS = bin/n23-db bin/n23-db-sync bin/n23-run
DOC_DEST = wrobell@dcmod.org:~/public_html/$(PROJECT)
USER_SHELL = zsh
